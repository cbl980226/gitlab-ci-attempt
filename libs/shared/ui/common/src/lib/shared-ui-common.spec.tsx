import { render } from '@testing-library/react';

import SharedUiCommon from './shared-ui-common';

describe('SharedUiCommon', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<SharedUiCommon />);
    expect(baseElement).toBeTruthy();
  });
});
